# xstore

This repository contains the metadata for xstore@testrun.org,
an example https://github.com/webxdc/store/ instance.
You can make a PR to store.ini
to publish your xdc app here.

Note that if you want to use this repository
to deploy the store yourself,
you need to add your own users and tokens
to the xdcget.ini,
the version in this repository is just a template.
